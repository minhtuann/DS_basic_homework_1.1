# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 10:08:17 2018

@author: Minh Tuấn1
"""

import datetime 

now = datetime.datetime.now()
print('Current date and time: ',now.strftime('%Y-%m-%d %H:%M:%S'))
#print(now.strftime('%Y-%m-%d %H:%M:%S'))

#==================================================================
#### Explain: 
# Import: Gọi thư viện để sử dụng. 
# datetime.datetime.now() : Lấy thời gian theo "now" định dạng theo milionsecond timestamp.
# Print: Hiển thị ra màn hình kết quả. 
# now.strftime: định dạng hiển thị kết quả theo Năm - Tháng - Ngày: Giờ - Phút - Giây.
#==================================================================

def sum(a,b): 
    sum = a +b 
    if sum in range(15,20):
        return 20 
    else: 
        return sum 
print(sum(10,6))
print(sum(1,1))

#=================================================================
### Explain: 
# def: tạo ra 1 function mới. 
# Bên dưới là cách thức function xử lý các tác vụ khi được gọi tới. 
# nếu kết quả tổng trong khoảng 15 đến 20 kết quả trả về 20, nếu khác khoảng này sẽ trả về kết quả bình thường. 
#===================================================================